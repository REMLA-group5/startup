start minikube

`minikube start --driver=virtualbox --cpus 2 --memory 4096`

install prometheus

`helm install prometheus prometheus-community/kube-prometheus-stack`

k8s

```
cd k8s
kubectl apply -f mymodels.yml
kubectl apply -f myweb.yml
kubectl apply -f ingress.yml
```

test pod

```
kubectl get pods
kubectl port-forward [pod] 8080
localhost:8080
localhost:8080/sms/
localhost:8080/metrics/
```

grafana

```
kubectl get pods
kubectl port-forward [grafana pod] 3000
```

username: `admin`

password: `prom-operator`
